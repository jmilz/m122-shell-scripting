# M122 Shell Scripting

git-Repository für Modul 122 - Abläufe mit einer Scriptsprache automatisieren

## Ablauf

1. Erstellen Sie sich einen **Account** bei [GitLab](https://gitlab.com/users/sign_up).  
   Verwenden Sie Ihre BBW-Adresse **vorname.nachname@lernende.bbw.ch** für das Login.
2. Erstellen Sie einen Fork von [M122 Shell Scripting](https://gitlab.com/bbwin/ref-card-03)  
   ![fork.png](assets/fork.png)  
   Ein **Fork** ist eine exakte Kopie des Ursprungsprojekts. Ab dann entwickelt es sich unabhängig; man entfernt, verändert oder ergänzt den Quellcode so, wie man es für nötig hält. Es ist möglich, aber eher unwahrscheinlich, dass ein Fork wieder mit seinem Ursprungsprojekt zusammengeführt wird.
3. Entscheiden Sie sich:
   1. Sie erstellen ein **public**-Repository. Jeder kann ihr Repository einsehen. Überlegen Sie sich ob Sie dies wollen. Wenn Sie mit public einverstanden sind müssen Sie nichts weiteres unternehmen.
   2. Sie erstellen ein **private**-Repository. Nur Sie haben Zugriff auf die Inhalte. In diesem Fall erfassen Sie in Gitlab unter *Manage* > *Members* > *Invite members* den User "**doxic**" mit der Rolle **Owner**  
      ![invite members](assets/invite.png)

